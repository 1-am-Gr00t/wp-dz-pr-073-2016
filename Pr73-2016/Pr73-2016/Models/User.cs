﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Pr73_2016.Models
{
    public class User
    {
        [MinLength(3)]
        [Key]
        public string Username { get; set; }
        [MinLength(8)]
        [RegularExpression("[0-9a-zA-Z]")]
        public string Password { get; set; }
        public string Name { get; set; }
        public string Lastname { get; set; }
        public Gender Gender { get; set; }
        public string Email { get; set; }
        public string DateOfBirth { get; set; }
        public UserType Type { get; set; }

        public bool Deleted { get; set; }
      
    }
}