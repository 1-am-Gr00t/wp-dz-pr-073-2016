﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace Pr73_2016.Models
{
    public class Consumers
    {
        public Dictionary<string, User> Shoppers { get; set; }
        public Consumers(string path)
        {
            Shoppers = new Dictionary<string, User>();
            path = HostingEnvironment.MapPath(path);

            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader streamReader = new StreamReader(stream);

            string line = "";
            while ((line = streamReader.ReadLine()) != null)
            {
                string[] consumerData = line.Split(';');

                User consumer = new User();
                consumer.Type = UserType.CONSUMER;

                consumer.Username = consumerData[0];
                consumer.Password = consumerData[1];
                consumer.Name = consumerData[2];
                consumer.Lastname = consumerData[3];
                if (consumerData[4] == "Male")
                {
                    consumer.Gender = Gender.MALE;
                }
                else if (consumerData[4] == "Female")
                {
                    consumer.Gender = Gender.FEMALE;
                }
                else
                {
                    consumer.Gender = Gender.UNSPECIFIED;
                }
                consumer.Email = consumerData[5];
                consumer.DateOfBirth = consumerData[6];
                consumer.Deleted = bool.Parse(consumerData[8]);

                Shoppers.Add(consumer.Username, consumer);
            }

            streamReader.Close();
            stream.Close();
        }
        public void AddConsumerToDb(User user, string path)
        {
            path = HostingEnvironment.MapPath(path);

            FileStream stream = new FileStream(path, FileMode.Append);
            StreamWriter streamWriter = new StreamWriter(stream);

            string line = "";

            line += $"{user.Username};{user.Password};{user.Name};{user.Lastname};{user.Gender.ToString()};{user.Email};{user.DateOfBirth};{user.Type};{user.Deleted.ToString()};";
            streamWriter.WriteLine(line);

            streamWriter.Close();
            stream.Close();
        }

    }
}