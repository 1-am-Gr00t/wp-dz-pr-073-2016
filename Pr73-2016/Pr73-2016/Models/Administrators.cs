﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace Pr73_2016.Models
{
    public class Administrators
    {
        public Dictionary<string, User> Admins { get; set; }

        public Administrators(string path)
        {
            Admins = new Dictionary<string, User>();
            path = HostingEnvironment.MapPath(path);

            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader streamReader = new StreamReader(stream);

            string line = "";
            while ((line = streamReader.ReadLine()) != null)
            {
                string[] adminData = line.Split(';');

                User admin = new User();
                admin.Type = UserType.ADMINISTRATOR;

                admin.Username = adminData[0];
                admin.Password = adminData[1];
                admin.Name = adminData[2];
                admin.Lastname = adminData[3];
                if (adminData[4]=="Male")
                {
                    admin.Gender = Gender.MALE;
                }
                else if (adminData[4]=="Female")
                {
                    admin.Gender = Gender.FEMALE;
                }
                else
                {
                    admin.Gender = Gender.UNSPECIFIED;
                }
                admin.Email = adminData[5];
                admin.DateOfBirth = adminData[6];
                admin.Deleted = bool.Parse(adminData[8]);

                Admins.Add(admin.Username, admin);
            }
        }
    }
}