﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Pr73_2016.Models
{
    public class Picture
    {
        [MinLength(1)]
        public string Name { get; set; }
        public Author Author { get; set; }
        public string YearPainted { get; set; }
        public string Technique { get; set; }
        public string Description { get; set; }
        [Range(1, 100000)]
        public int Price { get; set; }
        public bool isOnSale { get; set; }

        public bool isDeleted { get; set; }
        [Required]
        public string GalleryName { get; set; }
        public bool isSold { get; set; }
    }
}