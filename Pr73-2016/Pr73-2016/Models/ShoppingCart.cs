﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pr73_2016.Models
{
    public class ShoppingCart
    {
        public User Consumer { get; set; }
        public List<Picture> SelectedPictures { get; set; }
        public DateTime DateOfShopping { get; set; }
        public int TotalPrice { get; set; }

        public ShoppingCart()
        {
            SelectedPictures = new List<Picture>();
            TotalPrice = 0;
        }


    }
}