﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace Pr73_2016.Models
{
    public class Galleries
    {
        public Dictionary<string, Gallery> Galls { get; set; }

        public Galleries(string path)
        {
            Galls = new Dictionary<string, Gallery>();
            path = HostingEnvironment.MapPath(path);

            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader streamReader = new StreamReader(stream);

            string line = "";
            while ((line = streamReader.ReadLine()) != null)
            {
                string[] galleryData = line.Split(';');

                Gallery gallery = new Gallery();

                gallery.Name = galleryData[0];
                gallery.Address = galleryData[1];

                string[] pictures = (galleryData[2]).Split('-');

                foreach (string picture in pictures)
                {
                    gallery.Pictures.Add(picture);
                }

                Galls.Add(gallery.Name,gallery);
            }
        }
    }
}