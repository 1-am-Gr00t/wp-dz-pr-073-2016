﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace Pr73_2016.Models
{
    public class Pictures
    {
        
        public Dictionary<string, Picture> Pics { get; set; }

        public Pictures(string path)
        {
            Pics = new Dictionary<string, Picture>();
            path = HostingEnvironment.MapPath(path);

            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader streamReader = new StreamReader(stream);

            string line = "";
            while ((line = streamReader.ReadLine()) != null)
            {
                string[] picData = line.Split(';');
                Author author = new Author();

                string[] autor = (picData[1]).Split(' ');
                author.Name = autor[0];
                author.Lastname = autor[1];

                Picture picture = new Picture();
                picture.Name = picData[0];
                picture.Author = author;
                picture.YearPainted = picData[2];
                picture.Technique = picData[3];
                picture.Description = picData[4];
                picture.Price = Int32.Parse(picData[5]);
                picture.isOnSale = bool.Parse(picData[6]);
                picture.isDeleted = bool.Parse(picData[7]);
                picture.GalleryName = picData[8];
                picture.isSold = bool.Parse(picData[9]);

                Pics.Add(picture.Name, picture);
                
            }

            streamReader.Close();
            stream.Close();
        }       
    }
}