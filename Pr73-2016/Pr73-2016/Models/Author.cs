﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pr73_2016.Models
{
    public class Author
    {
        public string Name { get; set; }
        public string Lastname { get; set; }
    }
}