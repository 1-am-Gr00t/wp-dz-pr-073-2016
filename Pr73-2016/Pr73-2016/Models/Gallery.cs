﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Pr73_2016.Models
{
    public class Gallery
    {
        [MinLength(1)]
        public string Name { get; set; }
        public string Address { get; set; }
        public List<string> Pictures { get; set; }

        public Gallery()
        {
            Pictures = new List<string>();
        }
    }
}