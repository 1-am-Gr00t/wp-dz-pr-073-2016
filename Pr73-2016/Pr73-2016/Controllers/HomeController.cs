﻿using Pr73_2016.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pr73_2016.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            Pictures pictures = (Pictures)HttpContext.Application["Pictures"];
            Galleries galleries = (Galleries)HttpContext.Application["Galleries"];

            ViewBag.pictures = pictures.Pics;
            ViewBag.galleries = galleries.Galls;
           
            return View();
        }

        public ActionResult Search(string type, string findItem)
        {
            Pictures pictures = (Pictures)HttpContext.Application["Pictures"];
            Galleries galleries = (Galleries)HttpContext.Application["Galleries"];
            ViewBag.galleries = galleries.Galls;
            Dictionary<string, Picture> pics = new Dictionary<string, Picture>();

           
            switch (type)
            {
                case "Name":
                    foreach (Picture p in pictures.Pics.Values)
                    {
                        if (p.Name == findItem)
                        {
                            pics.Add(p.Name, p);
                        }
                    }
                    break;
                case "Tecnique":
                    foreach (Picture p in pictures.Pics.Values)
                    {
                        if (p.Technique.Contains(findItem))
                        {
                            pics.Add(p.Name, p);
                        }
                    }
                    break;
                case "Price":
                    if (findItem == "")
                    {
                        ViewBag.pictures = pictures.Pics;
                    }
                    else
                    {
                        string[] prices = findItem.Split('-');
                        int lowEnd = Int32.Parse(prices[0]);
                        int highEnd = Int32.Parse(prices[1]);

                        foreach (Picture p in pictures.Pics.Values)
                        {
                            if (p.Price > lowEnd && p.Price < highEnd)
                            {
                                pics.Add(p.Name, p);
                            }
                        }
                    }
                    
                    break;
                default:
                    ViewBag.pictures = pictures.Pics;
                    break;
            }


            ViewBag.pictures = pics;
            return View("Index");
        }

        public ActionResult Sort(string type, string orderingBy)
        {
            Pictures pictures = (Pictures)HttpContext.Application["Pictures"];
            Galleries galleries = (Galleries)HttpContext.Application["Galleries"];
            ViewBag.galleries = galleries.Galls;
            Dictionary<string, Picture> pics = new Dictionary<string, Picture>();
            
            switch (type)
            {
                case "Name":
                    if (orderingBy == "ascending")
                    {
                        pics = pictures.Pics.OrderBy(p => p.Value.Name).ToDictionary(p => p.Key, p => p.Value);
                    }
                    else
                    {
                        pics = pictures.Pics.OrderByDescending(p => p.Value.Name).ToDictionary(p => p.Key, p => p.Value);
                    }
                    break;
                case "Technique":
                    if (orderingBy == "ascending")
                    {
                        pics = pictures.Pics.OrderBy(p => p.Value.Technique).ToDictionary(p => p.Key, p => p.Value);
                    }
                    else
                    {
                        pics = pictures.Pics.OrderByDescending(p => p.Value.Technique).ToDictionary(p => p.Key, p => p.Value);
                    }
                    break;
                case "Price":
                    if (orderingBy == "ascending")
                    {
                        pics = pictures.Pics.OrderBy(p => p.Value.Price).ToDictionary(p => p.Key, p => p.Value);
                    }
                    else
                    {
                        pics = pictures.Pics.OrderByDescending(p => p.Value.Price).ToDictionary(p => p.Key, p => p.Value);
                    }
                    break;

                default:
                    break;
            }
            ViewBag.pictures = pics;
            return View("Index");
        }

        public ActionResult Register(string Name, string Lastname, string Username, string Gender, string DateOfBirth,
            string Email, string Password)
        {

            if (Username != null) //if key is null
            {
                if (Username.Length < 3 || Password.Length < 8) //if user and pass are wrongly entered
                {
                    return View("ErrorPage");
                }
                Consumers consumers = (Consumers)HttpContext.Application["Consumers"];

                User user = new User();

                user.Deleted = false;
                user.Type = UserType.CONSUMER;
                user.Name = Name;
                user.Lastname = Lastname;
                user.Username = Username;
                user.DateOfBirth = DateOfBirth;
                user.Email = Email;
                user.Password = Password;
                user.Deleted = false;

                consumers.Shoppers.Add(user.Username, user);

                HttpContext.Application["Consumers"] = consumers;

                Session["User"] = user;

                consumers.AddConsumerToDb(user, "~/App_Data/Users.txt");

                return RedirectToAction("LogIn");
            }
            return View();
        }

        public ActionResult LogIn(string Username, string Password)
        {
            if (Username!=null && Password !=null)
            {
                Consumers consumers = (Consumers)HttpContext.Application["Consumers"];

                User user = new User();

                if (!consumers.Shoppers.ContainsKey(Username))//if username is valid
                {
                    return View("ErrorPage");
                }
                user = consumers.Shoppers[Username];
                if (user.Deleted) //check if user is deleted by admin
                {
                    return View("ErrorPage");
                }
                if (user.Password == Password) //if password is valid
                {
                    Session["User"] = user;

                    return RedirectToAction("Index");
                }
                else
                {
                    return View("ErrorPage");
                }
                
            }
            return View();
        }
        public ActionResult LogOut()
        {
            Session["User"] = null;
            return RedirectToAction("Index");
        }

        public ActionResult ErrorPage()
        {
            return View();
        }
    }
    
}