﻿using Pr73_2016.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace Pr73_2016.Controllers
{
    public class ShoppingCartController : Controller
    {
        // GET: ShoppingCart
        public ActionResult Index()
        {
            ShoppingCart sc = (ShoppingCart)Session["sc"];
            ShoppingCart purchaseHistory = (ShoppingCart)Session["purchaseHistory"];

            if (purchaseHistory == null)
            {
                purchaseHistory = new ShoppingCart();
                Session["purchaseHistory"] = purchaseHistory;
            }
            if (sc == null)
            {
                sc = new ShoppingCart();
                Session["sc"] = sc;
            }
            ViewBag.sc = sc;
            return View();
        }

        // POST: ShoppingCart/Add
        public ActionResult Add(ShoppingCartFormParams pictureName)
        {           
            Pictures pictures = (Pictures)HttpContext.Application["Pictures"];
            ShoppingCart sc = (ShoppingCart)Session["sc"];
            
            if (sc == null)
            {
                sc = new ShoppingCart();
                Session["sc"] = sc;
            }
            if (!pictureName.Name.Equals(""))
            {
                sc.Consumer=(User)HttpContext.Session["User"];
                sc.SelectedPictures.Add(pictures.Pics[pictureName.Name]);
                sc.TotalPrice += pictures.Pics[pictureName.Name].Price;
                sc.DateOfShopping = DateTime.Now.Date;
            }
            ViewBag.sc = sc;
            
            return View("Index"); 
        }

        public ActionResult ShoppingHistory()
        {
            Pictures pictures = (Pictures)HttpContext.Application["Pictures"];

            ShoppingCart sc = (ShoppingCart)Session["sc"];
            Session["sc"] = null;

            for (int i = 0; i < sc.SelectedPictures.Count - 1; i++)
            {
                for (int j = i+1; j < sc.SelectedPictures.Count; j++)
                {
                    if (sc.SelectedPictures[i].Name==sc.SelectedPictures[j].Name)
                    {
                        return View("ErrorPage");
                    }
                }
            }

            ShoppingCart purchaseHistory = (ShoppingCart)Session["purchaseHistory"];
            if (purchaseHistory == null)
            {
                purchaseHistory = new ShoppingCart();
                Session["purchaseHistory"] = purchaseHistory;
            }

            if (sc == null || sc.SelectedPictures == null || sc.SelectedPictures.Count == 0)
            {
                return View("ErrorPage");
            }
            foreach (Picture p in sc.SelectedPictures)
            {
                purchaseHistory.SelectedPictures.Add(p);
                purchaseHistory.TotalPrice += p.Price;

                pictures.Pics[p.Name].isSold = true;
                pictures.Pics[p.Name].isDeleted = true;
                pictures.Pics[p.Name].isOnSale = false;
            }
            purchaseHistory.DateOfShopping = sc.DateOfShopping;
            purchaseHistory.Consumer = sc.Consumer;

            HttpContext.Application["Pictures"] = pictures;

            ViewBag.purchaseHistory = purchaseHistory;
            return View();
        }
        public ActionResult ErrorPage()
        {
            return View();
        }
    }
}