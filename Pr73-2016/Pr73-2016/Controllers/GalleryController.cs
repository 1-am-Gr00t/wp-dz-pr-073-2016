﻿using Pr73_2016.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pr73_2016.Controllers
{
    public class GalleryController : Controller
    {
        // GET: Gallery
        public ActionResult Index(GalleryFormParams gallery)
        {
            Pictures pictures = (Pictures)HttpContext.Application["Pictures"];
          
            ViewBag.g = gallery;
            ViewBag.ps = pictures;

            return View();
        }
       
    }
}