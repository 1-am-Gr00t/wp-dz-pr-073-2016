﻿using Pr73_2016.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Pr73_2016
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            //Load pics and galleries and users to mem to be global in app
            Pictures pictures = new Pictures("~/App_Data/Pictures.txt");
            HttpContext.Current.Application["Pictures"] = pictures;

            Galleries galleries = new Galleries("~/App_Data/Galleries.txt");
            HttpContext.Current.Application["Galleries"] = galleries;

            Administrators administrators = new Administrators("~/App_Data/Administrators.txt");
            HttpContext.Current.Application["Administrators"] = administrators;

            Consumers consumers = new Consumers("~/App_Data/Users.txt");
            HttpContext.Current.Application["Consumers"] = consumers;
        }
    }
}
